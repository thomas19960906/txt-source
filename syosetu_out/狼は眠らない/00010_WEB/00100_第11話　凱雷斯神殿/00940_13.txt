「等，等等。」
「等一下，冒険者雷肯！」
「卡西斯神官。該怎麼辦才好。」

卡西斯眼神動搖，仰望著雷肯。

「雷肯。艾達也能使用跟剛才一樣的招數是吧。」

原本還期待，看了雷肯的〈回復〉後，會把目標切換到雷肯身上，看來也沒那麼簡單。

「不一樣。威力不高，也控制得不好。但是，本質是相同的。」
「那麼，我代表本神殿，認可冒険者艾達的〈回復〉是凱雷斯大神的祝福，並提議接納冒険者艾達為本神殿的見習神官。」
「我贊同卡西斯神官的提議。這會是新同胞的誕生。」
「讓那招數成為我們神殿之物。當然是贊成的。」
「這會是即戰力。不，總有一天會向更上方邁進。我也大大贊成。」

只有帕吉爾神官表示了稍微不同的判斷。

「並沒有證明冒険者艾達的招數是凱雷斯大神的祝福。但是，如果本人和師傅同意的話，就贊成接納為本神殿的見習神官。」
「提議以四對一被承認了。冒険者艾達。你將被迎接為本神殿的見習神官。」
「我拒絶。」
「雷肯。沒在問你。已經沒你的事了。把艾達留下，離開吧。」

真是的，結果還是變成這樣了嗎，雷肯想著。

但也不覺得這齣猴戲毫無意義。
有聽從神殿的傳喚，誠實地回答了質問。在此前提下，神殿無視了這邊的意志打算束縛，而這邊並沒有過錯。

有為了留在這城鎮，做最大限度的努力。這一點應該有傳達給艾達了。然後也讓她領會到了，神官們有多不講理，神殿是多恐怖的場所。大概也學習了點辯論的方式。雖然在這方面，雷肯自己也算不上成熟，沒能給出很優質的示範就是了。

（那麼，回去吧）
（話說回來真是累阿）
（所以才討厭人類的世界）
（想去探索迷宮阿）

「艾達。走吧。」
「嗯。」

艾達伸出了手。
雷肯握住了她的手。
宛如握住公主大人的手一般。

「是打算去哪，雷肯。」
「離開城鎮。」
「你以為出得去嗎？哈哈哈。離開也無妨。如果你們的家族怎樣也無所謂的話。」
「我跟艾達都沒有家族。」
「有友人吧。那些人會沒辦法在這城鎮裡居住喔。」

帕吉爾神官出言指責。

「適可而止，卡西斯神官。你的話語，完全不是神官該說出口的。」
「能麻煩老人家安靜嗎。」

帕吉爾神官以嚴厲的眼神盯著卡西斯神官一陣子後，轉身從後方的門退出了。

「卡西斯神官。你沒好好調查我們的事情啊。我們是在最近才來到這城鎮的。沒什麼友人。硬要說的話就只有傑利可。」
「傑利可？」
「是隻猿猴喔。很有男人味的長腕猿。」

雷肯邊說著，邊用左手拉著艾達的手，用右手推開了門。
從後方傳來了像是要叫破喉嚨的大喊。

「神的士兵們！把那無禮之人拘束起來！」

三位神殿兵趕了過來，對著雷肯舉槍。
雷肯握著艾達的手，繼續前進。
三位神殿兵舉著槍向後退。

「在磨蹭什麼！男的殺了也無妨。女的要活捉起來！」

雷肯放開了艾達的手。

神殿兵們突進而來。
也不是需要特地從〈收納〉拿出劍來對付的對手，就直接徒手擋開三支槍，並各給予了一擊。
三人被打飛，撞在牆壁和地板上。
又通過一扇門後，來到了細長的走廊上。走廊邊有一扇門，站著警衛。

「請問要去哪裡？」
「要回去。」
「只有你們嗎？帶路的人呢？」
「來的時候有人帶路，但現在沒有。我走了。」
「阿。」

似乎有什麼話想說，但沒有硬要留下雷肯。

在通道轉了好幾次彎後，終於抵達了通往大廳的門。
雖然一定有更近的路線，但順著帶路時的路線走回去就會這樣。〈立體知覺〉沒辦法知道門有沒有上鎖，也沒辦法一次俯瞰很大的建築物。並非能找出最適當的路線的能力。

以前，和雷肯一同探索迷宮的冒険者之中，有個男人就算是在第一次經過的道路，也不會迷路。那個男人，似乎有著能看清地形和捷徑的能力。

（等等。知覺系有個叫〈圖化〉的魔法。那是怎樣的魔法）

打開了通向大廳的門，來到了祭壇旁。
和來的時候相同，有許多參拜者。但是，只有兩三人深陷於祈禱之中，其他人則眼裡帶著某種恐懼，看著雷肯。

雷肯從大廳的右側走向入口。艾達跟在後頭。參拜者們遠離了雷肯。
不過，有個男人接近了雷肯，並小聲搭話。

「雷肯先生。剛，剛剛，卡西斯神官帶著神殿騎士、神官和神殿兵，從正門出去了。說是要從正門前迎擊。你到底做了些什麼？」

雷肯想起了這男的是誰。
是長腕猿的調教師。帕雷多的主人。名字忘了。

「神殿想接走這邊的艾達，就拒絶了。」
「誒。」

雷肯丟著這回不出話的人不管，直接走向正門。〈立體知覺〉捕捉到艾達向男人低頭致意。
兩扇門和來的時候相同，大大敞開著。

石階下方有十位神殿兵站成半圓形，舉著槍。
後方有兩個身穿漂亮鎧甲的男人。沒有拔劍。這兩人都相當有實力。
旁邊站著兩位神官。兩人的魔力量都不少。

然後，卡西斯神官也在。戴著剛才沒戴的首飾。大概是什麼恩寵品或魔道具吧。握著杖。能感受到比剛才更強的魔力。
雷肯一臉這與我無關，走下了石階。
卡西斯神官舉起杖，詠唱了咒文。大概已經事先詠唱過準備詠唱了吧。

「〈硬直〉！」

雷肯感覺身體麻痺了起來。踏下去的腳在一瞬間停下了。但下一瞬間，身體的異常便消失而去，雷肯若無其事地將腳踏下石階。

「什，什麼？」

石階的階段數是九階。九大神也是，對這国家而言，九可能代表著特別的意義。想著這些事情，雷肯走下了階梯。艾達接著下來。
卡西斯神官詠唱著什麼咒文。是下個魔法的準備詠唱吧。雷肯很親切地，站著等待準備詠唱結束。

「〈睡眠〉！」

一瞬，意識被白霧蒙蔽，但異常很快就被抵銷了。

這個〈睡眠〉和馬拉奇斯使用的魔法相同。但記得初級得要碰觸對方的身體才能施展。也就是說，能從這種距離擊出的卡西斯神官，是中級以上的使用者吧。
但是，馬拉奇斯的更優秀。當時，在一瞬間失去了意識。卡西斯神官的魔法的效果很遲鈍。然而，卡西斯神官的魔力量遠勝了許多，剛才的兩個魔法中包含的魔力，也多得驚人。

也就是說，魔力量的大小，並不一定與魔法的技術一致。反過來講，魔力不多不代表不擅長魔法。今後也得多加註意。
而且，仔細想想的話，馬拉奇斯沒有準備詠唱。是在聊天時，很自然地發動了〈睡眠〉。靠著訓練，是能辦到這種事的。這就是所謂大意不得。

「〈混亂〉！」

一瞬，意識變得混濁，但雷肯在短時間內就取回了正常的判別力。
這也跟〈硬直〉相同，是精神系的中級魔法沒錯。而且包含的魔力量也多得讓人無語。

「盡是引起異常狀態的精神系魔法阿。卡西斯神官。」

作為聖職者，這又是怎麼回事呢。
又或者該說，真不虧是聖職者嗎。

「怎，怎麼可能。為什麼沒有效。怎麼可能會有這種事。」

雷肯向前走出一步。
十位神殿兵迅速地舉起槍，指向雷肯。
此時，雷肯的心中，突然湧出玩樂的心情。

想到了個想嘗試的事。
失敗也無妨。
成功的話，會很有趣。
以〈立體知覺〉確認十支槍的槍尖位置。幾乎是靜止的。能行。

「〈火矢〉。」

詠唱了咒文的雷肯胸前發出了光，十支槍的槍尖被擊滅了。雖然八支〈火矢〉就那樣消滅了，但另外兩支〈火矢〉落在了士兵們的腳邊。

「喔喔。」
「嗚哇。」

神殿兵們被嚇了一跳，往後退開。

「什，剛才的，剛才的是什麼。」

卡西斯神官陷入驚慌。
兩位神殿騎士拔出了劍。

（好劍）
（然後，好鬥氣）

愉快戰鬥的預感，讓雷肯的臉上不自覺地浮出笑容。
雷肯用左手拉外套，右手伸入內側，從〈收納〉拔出了〈拉斯庫之劍〉。
接著，對眼前的敵軍，放出了全力的殺氣。

最前列的十位神殿兵，宛如受到了強大魔獸的〈威壓〉，硬直了。
神殿騎士兩人，無意間止住了向前踏出的腳。
神官一人中斷了詠唱，另一人的杖掉了下來。

卡西斯神官張大了雙眼，顫抖著。
雷肯自然地垂下握著劍的右手，向前踏出一步。

難纏的毫無疑問是那兩位騎士，但雷肯的直覺告訴他，應該先打倒兩位神官。

（等等喔）
（有說過不能殺神官）
（雖然麻煩但也沒轍）
（把最前方的士兵踢飛撞到騎士身上）
（衝到左側把兩個神官的下顎打碎吧）
（對卡西斯神官的肚子揍一拳後）
（繞到右側把兩個騎士的右腕砍斷）
（要怎麼正式料理卡西斯神官之後再想）

此時，響起了平靜又凜然的女聲。

「你們在做什麼。」