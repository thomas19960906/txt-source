從王都維斯特露亞遠道而來，剛剛到達馬路特這個偏僻城市，一下子就變成這樣

「學院」首席諾埃爾・克魯傑一邊看著眼前正要消失的某国商人的背影，一邊確認自己的長袍的狀態，思考著這個問題

剛剛撞到商人之後，聽到了「叮」的一聲，長袍的魔術似乎受到了某種干涉。試著確認了一下，並沒有完全損壊，不過，上面的魔力已經相當程度的衰弱了
當然，我知道長袍是學院的工匠精心製作的，不會那麼容易被破壊

即便如此，事實就擺在眼前
至少，在我們到達馬路特的時候，長袍還是正常的。而且，抵達馬路特後到現在為止，如果說發生了什麼奇怪的事情，也就是被那邊的商人撞到了一下而已

從邏輯上判斷，犯人十有八九就是那個商人沒錯
一般來說，發生這種情況的話，應該去報告給學院的教授，讓他們代為發出譴責。或是自己將其叫住，穩妥的進行商議

但是這次，事態的發展卻未能如願
以結果來說，和商人發生了口角，引起了周圍的視線
失敗了啊

平時就常被人說，你說話的語氣有點自大，要稍微控制一下
為了避免這樣的事情，外出的時候自己也會特別注意
然而，之所以會變成這樣，是因為最近生活壓力太大的緣故吧
雖然找藉口也沒什麼用⋯⋯

事已至此，可供選擇的選項就很少了
但，自己的主張是沒有錯的⋯⋯商人似乎也有些心虛，從他那過剩的反駁中可以看出
如果只是普通人⋯⋯⋯比如這一帶露天攤販或店舖裡的平民們，或許無法看出其中的細微差別

但是諾埃爾家，是相當有權勢的貴族家系
具體來說，克魯傑家族擁有亞蘭王国的伯爵爵位
說起這份權勢，老實說，心裡很難平靜⋯⋯從前，這可是宰相輩出的名門

即是是如今，也仍與商人進行著大規模的交易
進行會談的時候，諾埃爾偶爾會被要求同席
當然，並不負責交涉，而是作為爵位的繼承人，為了積累經驗，以及記住商人的臉等理由出席的
因此，近幾年來，也能夠在某種程度上理解商人們的細微表情了。以此進行判斷，站在眼前的這個商人，非常奇怪。這就是諾埃爾得出的結論

話雖如此，在這之前還有個問題
他是如何損壊長袍的呢？
想不通

雖然看上去不是故意的，但商人的情緒相當高漲，稍微煽動一下的話，應該就會露出破綻。但那畢竟是陌生的商人
沒能付諸行動，事態陷入了僵局
現在，也可以選擇暫時撤退，由學院方面發起正式抗議

學院的影響力很大，而且，只要仔細檢查長袍，諾埃爾的主張便能得到證明
但那樣一來，感覺這個商人就會消失無蹤了
在這裡放他走不是一步好棋
雖然這只是直覺⋯⋯

正想著這些事情，突然，一個女孩從人群中衝了出來

愛麗澤・喬治
是在學院僅次於諾埃爾的次席學生，將來有望的少女
話雖如此，她的性格有些麻煩
是正義感相當強烈，會毫不猶豫付諸行動的類型

雖然這也不能算錯，但在這個場合任由她行動的話，只會讓事態進一步複雜化
本來是諾埃爾和商人間的爭吵，不知不覺間，就變成了諾埃爾和愛麗澤間的爭吵
瞥了商人一眼，他正在悄悄左顧右盼，好像想要找空隙溜走一樣

有些可疑啊
果然，是有什麼陰謀⋯⋯但是，目的是什麼呢？
雖然他似乎是故意破壊了諾埃爾的長袍，卻想不出可能的理由
結果，為了爭取時間，和愛麗澤吵了起來。嘛，這樣應該也不錯

正這樣打算著，這次又有別的人從人群中擠了出來，阻止了諾埃爾和愛麗澤的口角

⋯⋯誰啊？

首先，諾埃爾是這樣想的
愛麗澤應該也是如此

但是，即便不認識對方，諾埃爾也能明白，她是個相當有實力的魔術師
反覆精煉的魔力，毫無窒礙的架構
平常的魔術師大概看不出來吧，不過我能夠明白，她已經做好了臨戰準備，可以隨時放出魔法來

諾埃爾雖說是學院首席，但說穿了也不過是個見習魔術師
因此，實力上大概也就是中等偏下的水準，之所以能夠明白這些，是因為在進入學院之前，擔任家教的帝国高位魔術師老爺子也曾經散發著同樣的氛圍

在抓到諾埃爾曠課的時候，就是這樣對他威壓的⋯⋯雖然當時還是想要反抗，但對方微笑著放了出魔術
當然，命中了的只是衝擊程度的水系魔法，但故意打偏的魔術卻顯現出了可怕的威力。現在想起來還覺得後背發涼

他打從心底明白，只要那個老頭願意的話，就能在一瞬間讓自己停止呼吸
真是不想在回憶起來的往事

可怕的是，眼前的女性正散發著同樣的氛圍
所以，即便她靠近過來，甚至抓起了自己的衣服，諾埃爾也不敢行動，連聲音也發不出來