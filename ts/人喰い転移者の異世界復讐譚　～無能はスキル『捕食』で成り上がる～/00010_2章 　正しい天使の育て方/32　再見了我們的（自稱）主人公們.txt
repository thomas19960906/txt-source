「嗚嘎啊啊啊啊啊啊啊啊啊！」

薩提尼太斯發出野獸般的咆哮逼近過來。
三洗的全身各處，都已經被發光的綠色物質侵蝕，他早已變成了完全失去理性的人。

「百合！」
「誒？」

因為他叫的是我的名字，所以原以為他瞄準的一定是烏璐緹歐——但薩提尼太斯微妙地改變了方向，以百合所驅使的伊莉提姆為目標。
不，或許他是理解到我要保護她所以才這樣做的嗎？
那個速度連烏璐緹歐和赫洛斯都比不上。
距離在一瞬間拉近，他揮動著巨大的魔法佩劍。

鏗！

我猛撲向她，用力撞開了她。
雖然是很粗暴的方法，但現在只有這個辦法了。

唰！

魔法佩劍斬開伊莉提姆剛才在的地方，撕裂著空氣。
但是，間不容髮地，薩提尼太斯以迅速的動作揮出下一個攻擊。
這次的目標是我。
我用希瓦吉擋住了橫掃過來的刀刃。
但是，察覺到的時候，下一個瞬間，烏璐緹歐就已經在空中飛舞著。
在擋住薩提尼太斯的魔法佩劍的那一瞬間，連站穩在現場的事都無法做到，被吹飛了。

「白詰，白詰！」

就只會說那個嗎，好噁心啊！
薩提尼太斯，比烏璐緹歐被吹飛的速度更快地接近了過來，並且揮下魔法佩劍。
我這個姿勢是無法迴避的。
當我做好要受傷的覺悟時，發現薩提尼太斯的背後被伊莉提姆緊緊抱住。
雖然當然馬上就被掙脫了，但百合也立即發動了回歸虛空。
伊莉提姆的分身在那裡當即爆炸，雖說是只有一瞬間，但薩提尼太斯也是踉蹌了一下。
不會放過這個機會的！
著地後，我稍稍調整一下姿勢就發動了金剛杵，從烏璐緹歐的胸部射出的一擊必殺的武裝，確實直接擊中了薩提尼太斯。

「咕嗚嗚嗚嗚嗚啊啊啊啊啊！」

但是，怎麼也看不到有效果。
雖然聽說過奧利哈魯鋼會增幅魔力，但是看他那個樣子，似乎HP也得到了相當多的提升。
這樣的話，能期望的就是MP耗盡所導致的無法行動。
如果能帶入持久戰的話……不，我不覺得我能和現在的薩提尼太斯進行持久戰。
赫洛斯和在空中飛著的兩架阿尼瑪也只是一直呆呆地站著，好像不打算幫忙。

「哦哦哦哦哦哦哦哦哦哦哦哦！白詰詰詰詰！」

都說了不要像技能名那樣一次又一次地喊我的名字啊。
薩提尼太斯這次從背部的推進器中噴出魔力，浮到空中。
即使是被侵蝕的現在，作為推進器的功能似乎也沒有丟失。
在空中飄著的薩提尼太斯將視線轉向烏璐緹歐，用本來是作為牽制用的頭部小型魔法槍開火了。
但是——把那個叫做牽制用的話，我覺得，有點勉強。

嘰咿咿咿咿咿……隆隆隆隆隆隆隆隆隆！

他所射出的小型魔法槍，有著每一槍都足以幹掉一架阿尼瑪斯的威力。
正因為它是精密性得到公認的薩提尼太斯，所以射擊也很精準。
沒有反擊的餘地，我暫且先專心迴避。
著彈點就像是被具有相當威力的兵器炸毀一樣被挖開了一大塊，於是地表上被炸出了許多的炸彈坑。
樹木飛揚，偶爾子彈僅僅是在烏璐緹歐的表面上掠過，我就感受到HP被一下子削去的感覺。
剩餘的HP為，14600/41480。
哈哈，比和赫洛斯戰鬥的時候減少得更多了。
剛才擋下佩劍的做法有那麼不合適嗎。
如果只用魔法佩劍和頭部的魔法槍就能削掉這麼多的話，那麼用那隻手拿著的中型的魔法槍的話——我的HP會被奪去多少呢。

「匕首導彈，匕首導彈！停下來，給我停下來啊！」

百合拚命地攻擊著空中的薩提尼太斯。
雖然那種威力看起來是不能湊效的，但是怎麼說呢——看見那個拚命的樣子就使我充滿了幹勁。
不能死，怎可能死，我還沒讓艾爾萊亞墮落，復仇也還沒結束，而且還有彩花的事！

「嘎啊啊啊啊啊啊啊啊！」

結果終於發出了完全是野獸般的叫喊聲的薩提尼太斯，架好了手裡拿著的槍。
然後扣下了扳機。

咿咿……砰！

所射出的魔力子彈，在我的視線確認到之前已經打中了背後的地面，炸裂開來。
是這樣子的嗎。
巨大的佩劍，威力高的小型魔法槍，然後是速度快得目光捕捉不到的中型魔法槍。
那麼，該如何應對這個呢。
薩提尼太斯再次扣下扳機。
我調整體勢，僅僅只是集中於迴避上——

砰！

「嗚……！」

注意到的時候，肩膀已經中彈了。
這不是迴避之類的問題，你說要怎樣做才能避開眼睛也看不清的東西啊。
現在的HP是6750/41480。
真的是糟糕了，吃下下一擊的話就完蛋了嗎。

「岬！」

百合一邊喊著我的名字一邊跑了過來。

「百合，離我遠點專心支援就可以了！」
「如果離開了就不能成為盾了！」
「伊莉提姆是不可能承受那傢伙的攻擊的！」
「如果岬死了的話就全都白費了，要死的話就先從我開始吧！」

真是的，明明不是吵架的時候。
百合頑固地不肯離開。
啊，這樣的話，兩人一起死也是可……不，是不可以的。
還是把她當作祭品逃走？
那是不行的，因為如果那樣做的話，我就不僅是不能原諒同班同學和王國，就連是自己我都不能原諒了。
兩人，有甚麼能讓兩人一起逃跑的方法——

「卡文汀！」

這時，赫洛斯的左臂發出了光芒。
射出的魔力塊向著薩提尼太斯一直線前進，從側面直擊了他。
或許果真是無法忽視這個攻擊吧，薩提尼太斯的視線轉向了赫洛斯。

「雖然不打算原諒白詰，但是現在的三洗君怎麼看都不是正常的樣子。讓我來支援吧！」

真是越來越像主角的傢伙，但是既然可以利用的話，就盡情利用他吧。
赫洛斯和薩提尼太斯開始戰鬥。

也許是因為看到了我被吹飛的緣故，他沒有挨近過去進行近距離的戰鬥。
一邊保持著距離，用克勞索拉斯狙擊腳邊來降低對手的移動速度，一邊用卡文汀給予傷害。
另一邊的薩提尼太斯，也一邊用頭部小型魔法槍堵住對手的退路，一邊用手上的魔法槍確實地削減著赫洛斯的HP。

我們也不是只在看著。
百合發動技能創造出自己的分身，瞄準薩提尼太斯的背部射出了兩份的匕首導彈。
我也在用甘狄拔進行支援。

但是，即便如此——薩提尼太斯也沒有露出害怕的樣子。
給予幾倍的傷害就可以了嗎，但說到底當真對他造成了傷害嗎。
對方太過頑強，甚至令我抱有這樣的疑問。
而且，越是攻擊的話，奧利哈魯鋼的侵蝕範圍就越大。
每次三洗還會「咕啊啊啊啊！」這樣痛苦地喊著。
但是，與痛苦的三洗相對，我認為薩提尼太斯不但沒有變弱，反而變得更強了。

「糟了——」

赫洛斯終於被拉近了距離，進入了巨大的魔法佩劍的攻擊範圍內。
雖然桂立即用斷鋼劍來防禦，但是結果和我的那個時候一樣——赫洛斯比拼力氣失敗，被吹飛，摔倒在地上。

「他要來這邊了哦，怎麼辦？」
「只能幹了吧……但是再吃下一發就完蛋了，有點難辦呢」

再次面向這邊的薩提尼太斯。
彷彿是要確實地命中般，他慢慢地擺好手中握著的魔法槍。
吃下了這發的話，就死定了。
即使我沒有死，百合也會死。
會是其中一方。
但是，我覺得選擇是沒有意義的。
因為不管哪一方的死，都和兩者的死亡是同義的。

「嗚哦哦哦哦哦哦哦哦哦哦！」

那個時候，被吹飛的桂大喊道。
他以幹勁和毅力縮短了本應不可能趕及的距離，為了保護我和百合而來到了我們的面前。
緊接著，薩提尼太斯射出了魔法槍。

「咕嗚……！但是，還能撐下去！」

以身體接住它的桂。
為甚麼要這麼拚命努力呢。
為了殺死同伴的我……不，是為了保護最喜歡的廣瀨所遺留下的百合嗎。

「只有我們的話，是阻止不了現在的三洗君的。我們一定會在下次見面的時候做出了結了，所以，現在就——！」

一邊用斷鋼劍接下魔法槍，一邊催促著我們逃跑的桂。
他的HP應該已經剩下沒多少了。
如果繼續在這裡與薩提尼太斯交戰的話，就算赫洛斯多強，也會有生命危險的。
我不想要那樣。

「桂君，我不會逃的。一起戰鬥吧」
「白詰，但是！」
「所以——能讓你，當一下誘餌嗎？」

我接近集中於正面的防禦上、身體動彈不了的赫洛斯的背後，用力地把它踢飛。

「……哈？」

我還是第一次聽到桂發出憨呆的聲音呢。
失去平衡的赫洛斯，踉蹌地步向前方。

「咕啊啊啊啊啊啊……白詰詰詰詰……！」

一見赫洛斯接近，薩提尼太斯就將魔法槍收起，拿出了佩劍。
要把槍再一次拿出來的話，便需要一點時間。
這樣一來，眼前最近的危機就過去了。

「等、等等，我不是白詰……呃！」
「嗚哦哦哦哦哦啊啊啊啊哦啊啊啊！」

唰！
佩劍揮了下來。
赫洛斯連防禦都做不到，以側腹硬接下了那個。

「咕，嗚哇啊啊啊啊啊啊啊啊啊！」

就這樣，他被吹飛得比剛才的時候更遠，往山下的方向消失了。
桂，我不會浪費你的犧牲的哦。
在那個將佩劍切換為魔法槍的間隔、那個桂豁出性命爭取得來的空隙裡，我立刻把高火力猛敲進去。

「金剛杵！」

從胸部射出的高能量砲。
接著我一邊射著頭部小型魔法槍，一邊將一隻手握著的可變魔法槍和另一隻手架著的甘狄拔交替地射出去。
伊莉提姆在本體和分身都放出匕首導彈的同時，只有分身向薩提尼太斯接近過去。
薩提尼太斯向著距離最近的分身，射出了魔法槍。

啪嚓！

雖然分身只是挨了一發就無法戰鬥了，但是由於百合發動了回歸虛空，其爆炸的風浪襲擊了薩提尼太斯。
此時我，看見它第一次踉蹌了一下。

「有效！」
「嗯，能行的，岬！」

一邊把全部火力擊進薩提尼太斯，一邊毫不懈怠地考慮下一手。
這邊是再挨下一發就會死的垂死狀態。
與此相對，我不清楚對方還剩下多少HP，因為只讓他踉蹌了一下而已所以也可以認為他仍然相當有餘裕。

思考、思索、考慮的結果是——如果對方HP是仍然相當有餘裕的話，我判斷我們是不可能贏的。
所以我不再考慮這種可能性了。

我考慮的只有，薩提尼太斯已經處於垂死的狀態，我們只需要再削減多一點HP的這種情況。
即使就這樣繼續只是進行遠距離攻擊，也只會很快便會變成魔法槍的獵物。
剩下的HP，需要用更加高的火力——希瓦吉和金剛杵來解決掉。
也就是說，我需要硬是進行近距離戰鬥。
為此，必須用高於薩提尼太斯的機動性來更加接近它。

「百合，我有個請求」
「甚麼？」

儘管聲音被爆炸聲所淹沒，但我總算是向百合傳達了作戰內容。

「不，不是吧？這太亂來了吧！」

當然沒有得到同意。
但是，只能這樣做了。

「要去了！」
「誒，啊，等一下啊！啊啊……真是的，會變成怎樣我可不管了啦！」

我沒聽回覆，就跑了出去。
一邊用雙手的甘狄拔和魔法槍射擊，一邊親自強行深入死地。
薩提尼太斯的魔法槍的瞄準對我窮追不捨。
我無法避開他追著我的視線，但是——對手應該不是，能追上無法預料的動作的超人。

「匕首導彈！」

大量的導彈從伊莉提姆的裙子裡釋放了出來。
著彈的地方不是薩提尼太斯，而是烏璐緹歐的後背。

「咕嗚……哦哦哦哦哦哦哦哦！」

被爆炸的風壓吹跑，機體一下子加速。
魔法槍的瞄準偏掉，射出的魔力掠過了烏璐緹歐的手臂。
感到一陣刺痛。
看來HP好像已經變成0了。
導彈和剛才的掠過的那一槍，將剩餘的HP全都削去了嗎。
但是，我沒死。
也沒有停下。
即是說——我贏了！

「希瓦吉！」

我原封不動地活用被吹飛的勢頭，將拳頭向發出綠色光芒的薩提尼太斯的臉頰上敲去。
再加上我把收在手臂上的手甲劍向前伸出，大大地削減了對方的障壁。
薩提尼太斯踉踉蹌蹌地後退。

「再來一下，希瓦吉！」

再加上另一邊的拳頭，使之進一步後退。

「最後，金剛杵！」

從胸部釋放出來的魔力，這一次，這一次——終於貫穿了薩提尼太斯的障壁，燒燬了裝甲。

「嘎哦哦哦哦哦哦啊啊哦哦哦哦哦哦！」

三洗發出格外痛苦的慘叫聲。
或許是因為失去了障壁導致沒有東西阻止它了嗎，奧利哈魯鋼的侵蝕速度正在不斷加速。

啪嘰，啪嘰……嘰嘰……。

「嗚各各哦哦哦，唧啦，吱，啊嘎嘎嘎，嘰，咕嗚嗚嗚嗚……」

是不是已經連掌管最低限度的記憶的部分都被吃掉了呢，他連我的名字都叫不出來了。
然後，到最後奧利哈魯鋼覆蓋了他的全身，就像是以薩提尼太斯為養分般開始膨脹起來。

「這是，怎麼回事……？」
「僅僅外表是礦石，實際上卻是生物呢」
「明明像寶石一樣漂亮，真令人不舒服啊」
「同感」

就這樣置之不理的話可不知道會發生甚麼。
我拿出可變魔法槍，向著持續膨脹的結晶體射擊。

咚噢噢！

事前從普拉娜絲那所聽到的弱點是，推進器本身是沒有耐久性的。
也就是說，奧利哈魯鋼本身是非常脆弱的東西。
這是僅僅在增幅魔力時才有意義的物質。
而在淪為區區的結晶的現在，它就連普通的一擊都抵抗不住。

啪哩哩哩哩！

奧利哈魯鋼碎裂開來，像雨一樣地散落著綠色的結晶。
可能是機能停止了，即使接觸它也沒有被侵蝕的樣子。

「哈啊……終於結束了呢」

靠近到我旁邊的百合，嘆了口氣說道。

「嗯，這次真的很難對付啊。沒想被區區三洗逼到這種地步了」
「說是三洗，但感覺上其實是奧利哈魯鋼呢。那其實，究竟是怎回事呢」

普拉娜絲，沒有對我們說過奧利哈魯鋼會出現像是發生在薩提尼太斯身上那般的失控。
不，說起來她的口吻像是說知道奧利哈魯鋼的存在也只是最近的事，所以她大概是不知道奧利哈魯鋼會失控的這件事的吧。
於是，就連測試部隊都不知道這件事。

大概，桂倖存下來回到王都卡普托之後，奧利哈魯鋼的失控被眾所周知的話，研究也會稍微停滯下來的……我想。

「說回來，岬的身體沒問題嗎？好像發出了很痛的聲音」
「可能是擦傷了吧，但是還有……」
「還有？」
「2人，剩下啊」
「……啊」

不知甚麼時候探知技能關掉了。
大概是因為遭受了多次攻擊的緣故吧。
正當我為了搜尋剩下的2人的位置而發動技能的時候——

「白詰啊！」
「去死吧吧吧吧吧！」

他們突然從森林中衝出來，冷不防地用魔法槍指著我。
在HP變成了0的現在，如果我被那個槍的射擊打中的話，受到致命傷是無可避免的了。

百合慌慌張張地想要保護我，但我用手臂制止了她。
伊莉提姆也消耗了很多。
HP本來就已經很少了，我不覺得它能承受那2發。

那麼要怎麼做才能讓兩人倖存下去呢，但是我已經沒有思考的閒暇了。
魔力很快便會從槍口中射出。
奪取生命的，殘酷的子彈。

在我的眼裡，一連串的光景——包括從他們的身後逼近的那些，看起來全部都如同慢動作一樣。
