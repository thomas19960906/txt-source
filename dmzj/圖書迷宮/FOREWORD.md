
PV
https://youtu.be/xel1-R9Am4Q

【作者簡介】

作者：十字 靜
輕小說作家、工程師、顧問。
以《圖書迷宮》通過第10屆MF文庫J新人賞三次選拔，並以本作出道。

插畫：しらび
跨足小說插畫、遊戲插畫等多項領域的插畫家。現居埼玉。
代表作包含《龍王的工作！》、《無彩限的幻影世界》等等。

